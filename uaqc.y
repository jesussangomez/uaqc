/*************************************************************************
					Compiler for the Simple language
***************************************************************************/
/*=========================================================================
		C Libraries, Symbol Table, Code Generator & other C code
=========================================================================*/

%{
#include <stdio.h> 	/* For I/O */
#include <stdlib.h> /* For malloc here and in symbol table */
#include <string.h> /* For strcmp in symbol table */
#include "../include/ST.h"		/* Symbol Table */
#include "../include/SM.h" 	/* Stack Machine */
#include "../include/CG.h"		/* Code Generator */
#define YYDEBUG 1 	/* For Debugging */
int errors;			/* Error Count */

/*-------------------------------------------------------------------------
			Install identifier & check if previously defined.
-------------------------------------------------------------------------*/

install ( char *sym_name, int type)
{
	symrec *s_flag, *s_timer, *s_in, *s_out;
	switch (type){
		case 0: 
			s_flag = getsym (sym_name);
			if (s_flag == 0)
				s_flag = putsym (sym_name);
			else { 
				//errors++;
				//printf( "%s is already defined\n", sym_name );
			}
			break;
		case 1: 
			s_timer = gettim (sym_name);
			if (s_timer == 0)
				s_timer = puttim (sym_name);
			else { 
				//errors++;
				//printf( "%s is already defined\n", sym_name );
			}
			break;
		case 2: 
			s_in = getin (sym_name);
			if (s_in == 0)
				s_in = putin (sym_name);
			else { 
				//errors++;
				//printf( "%s is already defined\n", sym_name );
			}
			break;
		case 3: 
			s_out = getout (sym_name);
			if (s_out == 0)
				s_out = putout (sym_name);
			else { 
				//errors++;
				//printf( "%s is already defined\n", sym_name );
			}
			break;
	}
}

/*-------------------------------------------------------------------------
					If identifier is defined, generate code
-------------------------------------------------------------------------*/
context_check( enum code_ops operation, char *sym_name, int type)
{
	symrec *identifier, *id_timer, *id_in, *id_out;
	switch(type){
		case 0:
			identifier = getsym( sym_name );
			if ( identifier == 0 )
			{ 
				//errors++;
				printf( "%s", sym_name );
				printf( "%s\n", " is an undeclared identifier" );
			}	
			else
				gen_code( operation, identifier->offset );
			break;
		case 1:
			id_timer = gettim( sym_name );
			if ( id_timer == 0 )
			{ 
				//errors++;
				printf( "%s", sym_name );
				printf( "%s\n", " is an undeclared identifier" );
			}	
			else
				gen_code( operation, id_timer->offset );
			break;
		case 2:
			id_in = getin( sym_name );
			if ( id_in == 0 )
			{ 
				//errors++;
				printf( "%s", sym_name );
				printf( "%s\n", " is an undeclared identifier" );
			}	
			else
				gen_code( operation, id_in->offset );
			break;
		case 3:
			id_out = getout( sym_name );
			if ( id_out == 0 )
			{ 
				//errors++;
				printf( "%s", sym_name );
				printf( "%s\n", " is an undeclared identifier" );
			}	
			else
				gen_code( operation, id_out->offset );
			break;
	}
}

%}

/*=========================================================================
							SEMANTIC RECORDS
=========================================================================*/

%union semrec			/* The Semantic Records */
{
	int intval; 		/* Integer values */
	char *id;			/* Identifiers */
}

/*=========================================================================
								TOKENS
=========================================================================*/

%start program
%token <intval> NUMBER 			/* Simple integer */
%token <id> BAN TIM	IN OUT		/* Simple identifier */
%token INIT END

/*=========================================================================
					GRAMMAR RULES for the Ladder Boolean language
=========================================================================*/

%%

program : 		/*variables*/
			INIT
				commands
			END		{ gen_code(END_PROGRAM, 0); YYACCEPT; }
			;

/*variables: 
	| variables definitions
	;

definitions: primary_var '=' constant
	;

primary_var:
	BAN { install($1); }
	;

constant:
	NUMBER
	;*/

commands: /* empty */
	| commands expr
	;

expr: assign_expr
	;

assign_expr: or_expr
	| assign_expr '=' or_expr { gen_code(ASSIGN, 0); }
	;
	
or_expr: and_expr
	| or_expr '+' and_expr { gen_code(OR, 0); }
	;
	
and_expr: not_expr
	| and_expr '*' not_expr { gen_code(AND, 0); }
	;

not_expr: primary
	| '/' primary { gen_code(NOT, 0); }

primary:
	BAN { install($1, 0); context_check(FLAG, $1, 0); }
	| TIM { install($1, 1); context_check(TIMER, $1, 1); }
	| IN { install($1, 2); context_check(INPUT, $1, 2); }
	| OUT { install($1, 3); context_check(OUTPUT, $1, 3); }
	| '(' expr ')'
	;

%%

/*=========================================================================
									MAIN
=========================================================================*/
int main( int argc, char *argv[] )
{
	extern FILE *yyin;
	++argv; --argc;
	yyin = fopen( argv[0], "r" );
	// yydebug = 1;
	errors = 0;
	yyparse ();
	if ( errors == 0 )
	{ 
		//print_code ();
		fetch_execute_cycle();
		printf("Compilacion correcta 0 errores...\n");
	}
	return 0;
}

/*=========================================================================
									YYERROR
=========================================================================*/
yyerror ( char *s ) /* Called by yyparse on error */
{
	errors++;
	printf ("%s\n", s);
}

/**************************** End Grammar File ***************************/
