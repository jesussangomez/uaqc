/***************************************************************************
							Symbol Table Module
***************************************************************************/
/*=========================================================================
								DECLARATIONS
=========================================================================*/
/*-------------------------------------------------------------------------
							SYMBOL TABLE RECORD
-------------------------------------------------------------------------*/
struct symrec
{
	char *name; /* name of symbol */
	int offset; /* data offset */
	struct symrec *next; /* link field */
};

typedef struct symrec symrec;

/*-------------------------------------------------------------------------
							SYMBOL TABLE ENTRY
-------------------------------------------------------------------------*/
symrec *identifier, *id_timer, *id_in, *id_out;

/*-------------------------------------------------------------------------
								SYMBOL TABLE
					Implementation: a chain of records.
------------------------------------------------------------------------*/
symrec *sym_table = (symrec *)0; /* The pointer to the Symbol Table */
symrec *tim_table = (symrec *)0; /* The pointer to the Symbol Table */
symrec *in_table = (symrec *)0; /* The pointer to the Symbol Table */
symrec *out_table = (symrec *)0; /* The pointer to the Symbol Table */

/*========================================================================
						Operations: Putsym, Getsym
========================================================================*/
symrec * putsym (char *sym_name)
{
	symrec *ptr;
	ptr = (symrec *) malloc (sizeof(symrec));
	ptr->name = (char *) malloc (strlen(sym_name)+1);
	strcpy (ptr->name,sym_name);
	ptr->offset = data_location(0);
	ptr->next = (struct symrec *)sym_table;
	sym_table = ptr;
	return ptr;
}

symrec * getsym (char *sym_name)
{
	symrec *ptr;
	for ( ptr = sym_table; ptr != (symrec *) 0; ptr = (symrec *)ptr->next )
		if (strcmp (ptr->name,sym_name) == 0)
			return ptr;
	return 0;
}

/*========================================================================
						Operations: Putsym, Getsym
========================================================================*/
symrec * puttim (char *sym_name)
{
	symrec *ptr;
	ptr = (symrec *) malloc (sizeof(symrec));
	ptr->name = (char *) malloc (strlen(sym_name)+1);
	strcpy (ptr->name,sym_name);
	ptr->offset = data_location(1);
	ptr->next = (struct symrec *)tim_table;
	tim_table = ptr;
	return ptr;
}

symrec * gettim (char *sym_name)
{
	symrec *ptr;
	for ( ptr = tim_table; ptr != (symrec *) 0; ptr = (symrec *)ptr->next )
		if (strcmp (ptr->name,sym_name) == 0)
			return ptr;
	return 0;
}

/*========================================================================
						Operations: Putsym, Getsym
========================================================================*/
symrec * putin (char *sym_name)
{
	symrec *ptr;
	ptr = (symrec *) malloc (sizeof(symrec));
	ptr->name = (char *) malloc (strlen(sym_name)+1);
	strcpy (ptr->name,sym_name);
	ptr->offset = data_location(2);
	ptr->next = (struct symrec *)in_table;
	in_table = ptr;
	return ptr;
}

symrec * getin (char *sym_name)
{
	symrec *ptr;
	for ( ptr = in_table; ptr != (symrec *) 0; ptr = (symrec *)ptr->next )
		if (strcmp (ptr->name,sym_name) == 0)
			return ptr;
	return 0;
}

/*========================================================================
						Operations: Putsym, Getsym
========================================================================*/
symrec * putout (char *sym_name)
{
	symrec *ptr;
	ptr = (symrec *) malloc (sizeof(symrec));
	ptr->name = (char *) malloc (strlen(sym_name)+1);
	strcpy (ptr->name,sym_name);
	ptr->offset = data_location(3);
	ptr->next = (struct symrec *)out_table;
	out_table = ptr;
	return ptr;
}

symrec * getout (char *sym_name)
{
	symrec *ptr;
	for ( ptr = out_table; ptr != (symrec *) 0; ptr = (symrec *)ptr->next )
		if (strcmp (ptr->name,sym_name) == 0)
			return ptr;
	return 0;
}

/************************** End Symbol Table **************************/