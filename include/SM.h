/***************************************************************************
							Stack Machine
***************************************************************************/
/*=========================================================================
							DECLARATIONS
=========================================================================*/
/* OPERATIONS: Internal Representation */
enum code_ops {FLAG, INPUT, OUTPUT, TIMER, AND, OR, NOT, ASSIGN, END_PROGRAM};

/* OPERATIONS: External Representation */
char *op_name[] = {"FLAG", "INPUT", "OUTPUT", "TIMER", "AND", "OR", "NOT", "ASSIGN", "END_PROGRAM"};

struct instruction
{
	enum code_ops op;
	int arg;
	int mem_alloc;
	int mem_address;
};

/* CODE Array */
struct instruction code[999];

/* RUN-TIME Stack */
int stack[999];

/*-------------------------------------------------------------------------
								Registers
-------------------------------------------------------------------------*/
int pc = 0;
struct instruction ir;
int ar = 0;
int top = 0;
char ch;

/* GenCode */
enum code_ops init_op;
int init_mem, init_address, init_arg, init_flag;
int type;
int ban_conf = 0;
int tim_conf = 0;
int in_conf = 0;
int out_conf = 0;

int push_set = 0;

FILE *fasm, *fhex;

void configuration_code()
{
	int i;
	fprintf( fasm, "#include \"Memory_Mapping.h\"\n\n" );
	// for(i = 0; i<=in_conf; ++i)
	// 	fprintf( fasm, "NOT\nALD 0x0000\nNOT\nARG16 din%d\n", i );
	for(i = 0; i<=out_conf; ++i){
		fprintf( fasm, "NOT\nALD\t\t0x0000\nNOT\nARG16\tdout%d\n", i );
		push_set += 2;
	}
	for(i = 0; i<=ban_conf; ++i){
		fprintf( fasm, "NOT\nALD\t\t0x0000\nNOT\nARG16\tdban%d\n", i );
		push_set += 2;
	}
}

/*=========================================================================
							Fetch Execute Cycle
=========================================================================*/
void fetch_execute_cycle()
{
	fasm = fopen("uaqc.asm", "w+");
	fhex = fopen("uaqc.hex", "w+");
	configuration_code();
	init_op = ASSIGN;
	do {
		/* Fetch */
		ir = code[pc++];
		/* Execute */
		switch (ir.op) {
			case ASSIGN :
				switch (type){
					case 0: fprintf( fasm, "MBIT%d\tdban%d\t-- Ban_%d\n", init_mem, init_address, init_arg ); break;
					case 1: fprintf( fasm, "MBIT%d\tdin%d\t-- Input_%d\n", init_mem, init_address, init_arg ); break;
					case 2: fprintf( fasm, "MBIT%d\tdout%d\t-- Output_%d\n", init_mem, init_address, init_arg ); break;
					case 3: fprintf( fasm, "MBIT%d\tdtim%d\t-- Timer_%d\n", init_mem, init_address, init_arg ); break;
				}
				//fprintf( fhex, "%02X\t\t--MBIT%d\ndir%d\t-- Variable_%d\n", init_mem + 19, init_mem, init_address, init_arg);
				// fprintf( fasm, "MBIT%d\tdir%d_1 dir%d_2\t-- Variable_%d\n", init_mem, init_address, init_address, init_arg );
				// fprintf( fhex, "%02X\t\t--MBIT%d\ndir%d_1\ndir%d_2\t-- Variable_%d\n", init_mem + 19, init_mem, init_address, init_address, init_arg);
				break;
			case FLAG :
				if(init_op != ASSIGN){
					fprintf( fasm, "LBIT%d\tdban%d\t-- Ban_%d\n", ir.mem_alloc, ir.mem_address, ir.arg );
					//fprintf( fhex, "%02X\t\t--LBIT%d\ndir%d\t-- Variable_%d\n", ir.mem_alloc + 12, ir.mem_alloc, ir.mem_address, ir.arg );
					// fprintf( fasm, "LBIT%d\tdir%d_1 dir%d_2\t-- Variable_%d\n", ir.mem_alloc, ir.mem_address, ir.mem_address, ir.arg );
					// fprintf( fhex, "%02X\t\t--LBIT%d\ndir%d_1\ndir%d_2\t-- Variable_%d\n", ir.mem_alloc + 12, ir.mem_alloc, ir.mem_address, ir.mem_address, ir.arg );
				}
				else{
					init_mem = ir.mem_alloc;
					init_address = ir.mem_address;
					init_arg = ir.arg;
					type = 0;
				}
				break;
			case INPUT :
				if(init_op != ASSIGN){
					fprintf( fasm, "LBIT%d\tdin%d\t-- Input_%d\n", ir.mem_alloc, ir.mem_address, ir.arg );
					//fprintf( fhex, "%02X\t\t--LBIT%d\ndir%d\t-- Variable_%d\n", ir.mem_alloc + 12, ir.mem_alloc, ir.mem_address, ir.arg );
					// fprintf( fasm, "LBIT%d\tdir%d_1 dir%d_2\t-- Variable_%d\n", ir.mem_alloc, ir.mem_address, ir.mem_address, ir.arg );
					// fprintf( fhex, "%02X\t\t--LBIT%d\ndir%d_1\ndir%d_2\t-- Variable_%d\n", ir.mem_alloc + 12, ir.mem_alloc, ir.mem_address, ir.mem_address, ir.arg );
				}
				else{
					init_mem = ir.mem_alloc;
					init_address = ir.mem_address;
					init_arg = ir.arg;
					type = 1;
				}
				break;
			case OUTPUT :
				if(init_op != ASSIGN){
					fprintf( fasm, "LBIT%d\tdout%d\t-- Output_%d\n", ir.mem_alloc, ir.mem_address, ir.arg );
					//fprintf( fhex, "%02X\t\t--LBIT%d\ndir%d\t-- Variable_%d\n", ir.mem_alloc + 12, ir.mem_alloc, ir.mem_address, ir.arg );
					// fprintf( fasm, "LBIT%d\tdir%d_1 dir%d_2\t-- Variable_%d\n", ir.mem_alloc, ir.mem_address, ir.mem_address, ir.arg );
					// fprintf( fhex, "%02X\t\t--LBIT%d\ndir%d_1\ndir%d_2\t-- Variable_%d\n", ir.mem_alloc + 12, ir.mem_alloc, ir.mem_address, ir.mem_address, ir.arg );
				}
				else{
					init_mem = ir.mem_alloc;
					init_address = ir.mem_address;
					init_arg = ir.arg;
					type = 2;
				}
				break;
			case TIMER :
				if(init_op != ASSIGN){
					fprintf( fasm, "LBIT%d\tdtim%d\t-- Timer_%d\n", ir.mem_alloc, ir.mem_address, ir.arg );
					//fprintf( fhex, "%02X\t\t--LBIT%d\ndir%d\t-- Variable_%d\n", ir.mem_alloc + 12, ir.mem_alloc, ir.mem_address, ir.arg );
					// fprintf( fasm, "LBIT%d\tdir%d_1 dir%d_2\t-- Variable_%d\n", ir.mem_alloc, ir.mem_address, ir.mem_address, ir.arg );
					// fprintf( fhex, "%02X\t\t--LBIT%d\ndir%d_1\ndir%d_2\t-- Variable_%d\n", ir.mem_alloc + 12, ir.mem_alloc, ir.mem_address, ir.mem_address, ir.arg );
				}
				else{
					init_mem = ir.mem_alloc;
					init_address = ir.mem_address;
					init_arg = ir.arg;
					type = 3;
				}
				break;
			case AND :
				fprintf( fasm, "AND\t\t\t\t-- AND\n" );
				fprintf( fhex, "02\t\t--AND\n" );
				break;
			case OR :
				fprintf( fasm, "OR\t\t\t\t-- OR\n" );
				fprintf( fhex, "01\t\t--OR\n" );
				break;
			case NOT :
				fprintf( fasm, "NOT\t\t\t\t-- NOT\n" );
				fprintf( fhex, "03\t\t--NOT\n" );
				break;
		}
		init_op = ir.op;
	}
	while (ir.op != END_PROGRAM);
	int i;
	for(i = 0; i < push_set; ++i)
		fprintf( fasm, "PUSHSET\n" );
	fprintf(fasm, "FIN\n" );
	//fprintf( fhex, "33\n33\n33\n33\n33\n33\n33\n33\n33\n33\n33\n33\nFF\n" );
}

/*************************** End Stack Machine **************************/