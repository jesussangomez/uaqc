/***************************************************************************
							Code Generator
***************************************************************************/
/*-------------------------------------------------------------------------
							Data Segment
-------------------------------------------------------------------------*/
int data_offset = 0; /* Initial offset */
int tim_offset = 0; /* Initial offset */
int in_offset = 0; /* Initial offset */
int out_offset = 0; /* Initial offset */

int data_location(int type) /* Reserves a data location */
{
	int data;
	switch(type){
		case 0: data = data_offset++; break;
		case 1: data = tim_offset++; break;
		case 2: data = in_offset++; break;
		case 3: data = out_offset++; break;
	}
	return data;
}

/*-------------------------------------------------------------------------
							Code Segment
-------------------------------------------------------------------------*/
int code_offset = 0; /* Initial offset */
int eight_bflag = 0;
int eight_tflag = 0;
int eight_iflag = 0;
int eight_oflag = 0;

int gen_label() /* Returns current offset */
{
	return code_offset;
}

int reserve_loc() /* Reserves a code location */
{
	return code_offset++;
}

/* Generates code at current location */
void gen_code( enum code_ops operation, int arg)
{
	code[code_offset].op = operation;
	code[code_offset].mem_alloc = 0;
	if(operation == FLAG){
		if(arg < 8 )
			eight_bflag = 0;
		if(arg >= 8 && arg < 16)
			eight_bflag = 1;
		if(arg >= 16 && arg < 24)
			eight_bflag = 2;
		if(arg >= 24 && arg < 32)
			eight_bflag = 3;
		if(arg >= 32 && arg < 40)
			eight_bflag = 4;
		if(arg >= 40 && arg < 48)
			eight_bflag = 5;
		// printf("%d %d %d\n", arg, arg - (eight_flag * 8), eight_flag);
		code[code_offset].mem_alloc = arg - (eight_bflag * 8);

		code[code_offset].mem_address = eight_bflag;
	}
	if(operation == TIMER){
		if(arg < 8 )
			eight_tflag = 0;
		if(arg >= 8 && arg < 16)
			eight_tflag = 1;
		if(arg >= 16 && arg < 24)
			eight_tflag = 2;
		if(arg >= 24 && arg < 32)
			eight_tflag = 3;
		if(arg >= 32 && arg < 40)
			eight_tflag = 4;
		if(arg >= 40 && arg < 48)
			eight_tflag = 5;
		code[code_offset].mem_alloc = arg - (eight_tflag * 8);

		code[code_offset].mem_address = eight_tflag;
	}
	if(operation == INPUT){
		if(arg < 8 )
			eight_iflag = 0;
		if(arg >= 8 && arg < 16)
			eight_iflag = 1;
		if(arg >= 16 && arg < 24)
			eight_iflag = 2;
		if(arg >= 24 && arg < 32)
			eight_iflag = 3;
		if(arg >= 32 && arg < 40)
			eight_iflag = 4;
		if(arg >= 40 && arg < 48)
			eight_iflag = 5;
		code[code_offset].mem_alloc = arg - (eight_iflag * 8);

		code[code_offset].mem_address = eight_iflag;
	}
	if(operation == OUTPUT){
		if(arg < 8 )
			eight_oflag = 0;
		if(arg >= 8 && arg < 16)
			eight_oflag = 1;
		if(arg >= 16 && arg < 24)
			eight_oflag = 2;
		if(arg >= 24 && arg < 32)
			eight_oflag = 3;
		if(arg >= 32 && arg < 40)
			eight_oflag = 4;
		if(arg >= 40 && arg < 48)
			eight_oflag = 5;
		code[code_offset].mem_alloc = arg - (eight_oflag * 8);

		code[code_offset].mem_address = eight_oflag;
	}
	code[code_offset++].arg = arg;
	ban_conf = eight_bflag;
	tim_conf = eight_tflag;
	in_conf  = eight_iflag;
	out_conf = eight_oflag;
}

/* Generates code at a reserved location */
void back_patch( int addr, enum code_ops operation, int arg )
{
	code[addr].op = operation;
	code[addr].arg = arg;
}

/*-------------------------------------------------------------------------
							Print Code to stdio
-------------------------------------------------------------------------*/
void print_code()
{
	int i = 0;
	while (i < code_offset) {
		printf("%3d: %-10s%4d\n",i,op_name[(int) code[i].op], code[i].arg );
		i++;
	}
}

/************************** End Code Generator **************************/
