/***************************************************************************
					Scanner for the Simple language
***************************************************************************/
/*=========================================================================
					C-libraries and Token definitions
=========================================================================*/

%{
#include <string.h> /* for strdup */
#include "../src/uaqc.tab.h" /* for token definitions and yylval */
%}

/*=========================================================================
							TOKEN Definitions
=========================================================================*/

DIGIT [0-9]
FLG [banBAN][0-9]*
TMR [timTIM][0-9]*
IN [inIN][0-9]*
OUT [outOUT][0-9]*

/*=========================================================================
		REGULAR EXPRESSIONS defining the tokens for the Simple language
=========================================================================*/

%%
init 			{ return(INIT); }
end 			{ return(END); }
{DIGIT}+ 		{ yylval.intval = atoi( yytext ); return(NUMBER); }
{FLG}+ 			{ yylval.id = (char *) strdup(yytext); return(BAN); }
{TMR}+ 			{ yylval.id = (char *) strdup(yytext); return(TIM); }
{IN}+ 			{ yylval.id = (char *) strdup(yytext); return(IN); }
{OUT}+ 			{ yylval.id = (char *) strdup(yytext); return(OUT); }
"*"				{ return ('*'); }
"+"				{ return ('+'); }
"="				{ return ('='); }
"/"				{ return ('/'); }
"("				{ return ('('); }
")"				{ return (')'); }
[ \t\n\r\f]+ 	{ /* eat up whitespace */ }
. 				{ yyerror("Invalid Character"); printf("%c\n",yytext[0]); }
%%

int yywrap(void){}

/************************** End Scanner File *****************************/
