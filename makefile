# Makefile: A simple makefile for uaqc.
default:
	bison -dv uaqc.y -o src/uaqc.tab.c 
	flex -o src/lex.yy.c uaqc.l
	gcc -c src/uaqc.tab.c -o obj/uaqc.tab.o
	gcc -c src/lex.yy.c -o obj/lex.yy.o
	gcc -o bin/uaqc obj/uaqc.tab.o obj/lex.yy.o
clean:
	rm src/*.* obj/*.*
