#ifndef MEMORY_MAPPING_H
#define MEMORY_MAPPING_H
 
#define dban0	0x0037
#define dban1	0x0039
#define dban2	0x003A
#define dban3	0x003B
#define dban4	0x003C
#define dban5	0x003D
#define dban6	0x003E
#define dban7	0x003F
#define dban8	0x0040
#define dban9	0x0041
#define dban10	0x0042
#define dban11	0x0043
#define dban12	0x0044
#define dban13	0x0045
#define dban14	0x0046
#define dban15	0x0047

#define din0	0x002D
#define din1	0x002E
#define din2	0x002F
#define din3	0x0030
#define din4	0x0031

#define dout0	0x0032
#define dout1	0x0033
#define dout2	0x0034
#define dout3	0x0035
#define dout4	0x0036

#define dtim0	0x0000
#define dtim1	0x0005
#define dtim2	0x000A
#define dtim3	0x000F
 
#endif